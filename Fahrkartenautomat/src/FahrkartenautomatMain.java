import java.util.Scanner;

class FahrkartenautomatMain {
  public static void main(String[] args)
  {
    Scanner tastatur = new Scanner(System.in);//Erstellen des Objekts tastatur der Klasse Scanner

    double zuZahlenderBetrag; //Deklaration als double
    double eingezahlterGesamtbetrag; //Deklaration als double

    while(true) {
      //Bestellung erfassen
      //-------------------
      zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);//Aufruf der Methode fahrkartenbestellungErfassung mit dem Objekt
      // tastatur der Klasse Scanner als Parameter. Anschließend wird der Rückgabewert der Methode der Variable
      // zuZahlenderBetrag zugewiesen

      // Geldeinwurf
      // -----------
      eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);//Aufruf der Funktion fahrkartenBezahlen mit den Parametern
      //zuZahlenderBetrag und dem Objekt Tastatur der Klasse Scanner. Danach wird der Variable der Rückgabewert dieser Methode zugewiesen.

      // Fahrscheinausgabe
      // -----------------
      fahrkartenAusgeben();//Aufruf der Funktion fahrkartenAusgeben

      // Rückgeldberechnung und -Ausgabe
      // -------------------------------
      rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);//Aufruf der Funktion rueckgeldAusgeben mit den
      // Parametern eingezahlterGesamtbetrag und zuZahlenderBetrag
      System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
              "vor Fahrtantritt entwerten zu lassen!\n" +
              "Wir wünschen Ihnen eine gute Fahrt.\n");
    }
  }

  public static double fahrkartenbestellungErfassen(Scanner scanner){

    double einzelpreis; //Deklaration einzelpreis
    byte ticketAnzahl; //Deklaration ticketAnzahl
    double gesamtpreis;// Deklaration gesamtpreis
    boolean ist_valide;//Deklaration ist_valide
    byte eingabe;//deklaration eingabe

    String fahrkartenbezeichnung[] ={"Einzelfahrschein Berlin AB",
            "Einzelfahrschein Berlin BC",
            "Einzelfahrschein Berlin ABC",
            "Kurzstrecke",
            "Tageskarte Berlin AB",
            "Tageskarte Berlin BC",
            "Tageskarte Berlin ABC",
            "Kleingruppen-Tageskarte Berlin AB",
            "Kleingruppen-Tageskarte Berlin BC",
            "Kleingruppen-Tageskarte Berlin ABC",

    };

    double fahrkartenpreise[]={2.90,3.3,3.6,1.9,8.6,9d,9.6,23.5,24.3,24.9};

    System.out.println("Fahrkartenbestellvorgang\n=========================\n");
    System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
    for(int iterator =0;iterator<fahrkartenbezeichnung.length;iterator++){
      System.out.printf(" %s [%.2f EUR] (%d)\n",fahrkartenbezeichnung[iterator],fahrkartenpreise[iterator],iterator+1);
    }
    System.out.println();

    einzelpreis=0;//Zuweisung einzelpreis mit dem Wert 0

    do{
      ist_valide=false;
      System.out.print("Ihre Wahl: ");
      eingabe = scanner.nextByte();//Initialisierung einzelpreis mit dem eingegebenen Wert des Benutzers
      if(eingabe>=0&&eingabe<=fahrkartenpreise.length){
        einzelpreis = fahrkartenpreise[eingabe-1];
      }else{
        ist_valide = true;
      }
    }while(ist_valide);


    System.out.print("Anzahl der Tickets: ");
    ticketAnzahl = scanner.nextByte();
    //Initialisierung der Variaable ticketAnzahl mit dem
    // Eingegebenen Wert des Benutzers in der Konsole


    gesamtpreis= einzelpreis*(double)ticketAnzahl;
    return gesamtpreis;//Rückageb des Gesamtpreises
  }

  public static double fahrkartenBezahlen(double zuZahlenderBetrag,Scanner scanner){

    double eingezahlterGesamtbetrag = 0.0; //Initialisierung eingezahlterGesamtbetrag mit 0.0
    double eingeworfeneMuenze;//Deklaration eingeworfene Münze
    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)//solange der bezahlte Betrag kleiner ist als der noch zu zahlende Betrag, führe die Schleife aus
    {
      System.out.printf("Noch zu zahlen: %.2f Euro ", (zuZahlenderBetrag - eingezahlterGesamtbetrag));//Ausgabe des offenen Betrages
      System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      eingeworfeneMuenze = scanner.nextDouble();//Benutzer wirft eine Münze ein
      eingezahlterGesamtbetrag += eingeworfeneMuenze;//Berechnung des bereits eingeworfenen Betrages
    }
    return eingezahlterGesamtbetrag;//Rückgabe des eingezahlten Gesamtbetrags
  }
  public static void fahrkartenAusgeben(){
    //Animation der Bearbeitung + Ausgabe der Tickets
    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++)
    {
      System.out.print("=");
      try {
        Thread.sleep(250);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    System.out.println("\n\n");
  }

  public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag,double zuZahlenderBetrag){
    double rueckgabebetrag;//Deklaration des rueckgabebetrags
    rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;//Berechnung der höhe des Restgeldes
    if(rueckgabebetrag > 0.0)//vergleich ob der Benutzer Restgeld bekommt
    {
      System.out.println("Der rueckgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
      System.out.println("wird in folgenden Münzen ausgezahlt:");
      //Vergleich ob der Restbetrag größer ist als die verglichene Münze und wenn true auswurf jener(für jede Münze von großem Wert nach kleinem Wert)
      while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
      {
        System.out.println("2 EURO");
        rueckgabebetrag -= 2.0;
      }
      while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
      {
        System.out.println("1 EURO");
        rueckgabebetrag -= 1.0;
      }
      while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
      {
        System.out.println("50 CENT");
        rueckgabebetrag -= 0.5;
      }
      while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
      {
        System.out.println("20 CENT");
        rueckgabebetrag -= 0.2;
      }
      while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
      {
        System.out.println("10 CENT");
        rueckgabebetrag -= 0.1;
      }
      while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
      {
        System.out.println("5 CENT");
        rueckgabebetrag -= 0.05;
      }
    }
  }
}
