import java.util.Scanner;

public class Volumen {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int modus;
    System.out.println("Was soll berechnet werden?\n 1 = Würfel; 2 = Quader;3 = Pyramide;4 = Kugel");
    modus = scanner.nextInt();
    double erg=0;
    switch (modus){
      case 1:
        erg= wuerfel(scanner);
        break;
      case 2:
        erg = quader(scanner);
        break;
      case 3:
        erg = pyramide(scanner);
        break;

      case 4:
        erg = kugel(scanner);
        break;
      default:
        break;
    }
    System.out.println("Das Ergebnis ist: " +erg);
    scanner.close();


  }

  public static double wuerfel(Scanner sc){
    double a = sc.nextDouble();
    return a*a*a;
  }
  public static double quader(Scanner sc){
    System.out.println("Bitte Laenge eingeben: ");
    double a = sc.nextDouble();
    System.out.println("Bitte hoehe eingeben: ");
    double b = sc.nextDouble();
    System.out.println("Bitte tiefe eingeben: ");
    double c = sc.nextDouble();
    return a*b*c;
  }
  public static double pyramide(Scanner sc){
    System.out.println("Bitte Seitenlaenge eingeben: ");
    double a= sc.nextDouble();
    System.out.println("Bitte hoehe eingeben: ");
    double h= sc.nextDouble();

    return (a * a * h / 3);
  }
  public static double kugel(Scanner sc){
    System.out.println("Bitte radius eingeben: ");
    double r = sc.nextDouble();
    return 4/3 * r*r*r * Math.PI;
  }
}
