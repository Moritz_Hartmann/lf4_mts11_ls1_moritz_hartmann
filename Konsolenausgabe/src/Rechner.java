import java.util.Scanner; // Import der Klasse Scanner
public class Rechner
{

  public static void main(String[] args) // Hier startet das Programm
  {

    // Neues Scanner-Objekt myScanner wird erstellt
    Scanner myScanner = new Scanner(System.in);

    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

    // Die Variable zahl1 speichert die erste Eingabe
    int zahl1 = myScanner.nextInt();

    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

    // Die Variable zahl2 speichert die zweite Eingabe
    int zahl2 = myScanner.nextInt();

    // Die Addition der Variablen zahl1 und zahl2
    // wird der Variable ergebnis zugewiesen.
    int ergebnis = zahl1 + zahl2;

    System.out.print("\n\n\nErgebnis der Addition lautet: ");
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);

    int ergebnis1 = zahl1 - zahl2;
    System.out.print("\nErgebnis der Subtraktion lautet: ");
    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis1);

    int ergebnis2 = zahl1 * zahl2;
    System.out.print("\nErgebnis der Multiplikation lautet: ");
    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis2);

    int ergebnis3 = zahl1 / zahl2;
    System.out.print("\nErgebnis der Division lautet: ");
    System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis3);

    myScanner.close();

  }
}
