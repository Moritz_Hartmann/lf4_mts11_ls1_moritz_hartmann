import java.util.Scanner;

public class Steckbrief {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.println("Guten Tag.Bitte geben sie ihren Namen ein");
    String name = scanner.nextLine();
    System.out.println("Bitte geben sie ihr Alter ein:");
    int alter = scanner.nextInt();
    System.out.println(name+" ist "+alter+" Jahre alt");
  }
}
