public class WeltderZahlen {

  public static void main(String[] args) {

    /*  *********************************************************

         Zuerst werden die Variablen mit den Werten festgelegt!

    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem
    byte anzahlPlaneten =  8;

    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 400000000000l;

    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000;

    // Wie alt bist du?  Wie viele Tage sind das?

    short alterTage =  7872;

    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000;

    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int flaecheGroessteLand = 17130000;

    // Wie groß ist das kleinste Land der Erde?
    float flaecheKleinsteLand = 0.44f;




    /*  *********************************************************

         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen

    *********************************************************** */
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);

    System.out.println("Anzahl der Sterne: "+anzahlSterne);
    System.out.println("Einwohner Berlin: " + bewohnerBerlin);
    System.out.println("Alter in Tagen " + alterTage);
    System.out.println("Gewicht in KG: " + gewichtKilogramm);
    System.out.println("Flaeche groesstes Land (Russland) in km^2= "+flaecheGroessteLand);
    System.out.println("Flaeche kleinstes Land (Vatikanstadt)in km^2: " + flaecheKleinsteLand);

    System.out.println(" *******  Ende des Programms  ******* ");

  }


}
