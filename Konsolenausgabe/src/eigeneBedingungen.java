import java.util.Scanner;

public class eigeneBedingungen {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Bitte Eingeben welche Aufgabe bearbeitet werden soll.\n 1 = Aufgabe 1.1; 2 = Aufgabe 1.2; 3 = Aufgabe 1.3; 4 = Aufgabe 1.4");
    int eingabe = scanner.nextInt();
    switch (eingabe) {
      case 1:
        aufgabe_1_1();
        break;
      case 2:
        aufgabe_1_2(scanner);
        break;
      case 3:
        aufgabe_1_3(scanner);
        break;
      case 4:
        aufgabe_1_4(scanner);
        break;
    }


  }
  public static void aufgabe_1_1(){
    System.out.println("Wenn ich müde bin, dann gehe ich schlafen");
  }

  public static void aufgabe_1_2(Scanner scanner){
    System.out.println("Bitte erste Zahl eingeben.");
    int zahl_1 = scanner.nextInt();
    System.out.println("Bitte zweite Zahl eingeben.");
    int zahl_2 = scanner.nextInt();
    //Aufgabe 1.2
    if(zahl_1==zahl_2){
      System.out.println("Die erste Zahl ist geich der zweiten Zahl");
    }
  }
  public static void aufgabe_1_3(Scanner scanner){
    System.out.println("Bitte erste Zahl eingeben.");
    int zahl_1 = scanner.nextInt();
    System.out.println("Bitte zweite Zahl eingeben.");
    int zahl_2 = scanner.nextInt();
    if(zahl_1<zahl_2){
      System.out.println("Die erste Zahl ist kleiner als die zweite Zahl");
    }
  }
  public static void aufgabe_1_4(Scanner scanner){
    System.out.println("Bitte erste Zahl eingeben.");
    int zahl_1 = scanner.nextInt();
    System.out.println("Bitte zweite Zahl eingeben.");
    int zahl_2 = scanner.nextInt();
    if(zahl_1>=zahl_2){
      System.out.println("Die erste Zahl ist größer gleich der zweiten Zahl");

    }else{
      System.out.println("Die zweite Zahl ist größer als die erste Zahl");
    }
  }


}
