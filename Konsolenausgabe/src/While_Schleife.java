import java.util.ArrayList;
import java.util.Scanner;
public class While_Schleife {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Bitte Aufgabennummer eingeben.");
        int eingabe = scanner.nextInt();
        switch(eingabe) {
            case 1 -> aufgabe_1(scanner);
            case 2->aufgabe_2(scanner);
            case 3->aufgabe_3(scanner);
            case 4->aufgabe_4(scanner);
            case 5->aufgabe_5(scanner);
            case 6->aufgabe_6(scanner);
            case 7->aufgabe_7();
            case 8->aufgabe_8(scanner);
        }
    }
    public static void aufgabe_1(Scanner scanner){
        int eingabe = scanner.nextInt();
        int i = 1;
        while(i<=eingabe){
            System.out.print(i);
            i++;
        }
        while(i>=1){
            System.out.print(i);
            i--;
        }
    }
    public static void aufgabe_2(Scanner scanner){
        int eingabe = scanner.nextInt();
        int erg=1;
        int i =1;
        while(i<=eingabe){
            erg*=i;
            i++;
        }
        System.out.println(erg);
    }
    public static void aufgabe_3(Scanner scanner){
        int eingabe = scanner.nextInt();
        int i = 0;
        int erg = 0;
        while(eingabe >0){
            erg += eingabe%10;
            eingabe = eingabe/10;

        }
        System.out.println(erg);
    }
    public static void aufgabe_4(Scanner scanner){
        System.out.println("Bitte den Startwert in Celsius eingeben: ");
        int start = scanner.nextInt();
        System.out.println("Bitte den Endwert in Celsius eingeben: ");
        int endwert = scanner.nextInt();
        System.out.println("Bitte die Schrittweite in Grad Celsius eingeben: ");
        int schrittweite = scanner.nextInt();

        while(start<=endwert){
            System.out.println(start+"C "+(start*1.8+32)+"F");
            start+=schrittweite;
        }
    }
    public static void aufgabe_5(Scanner scanner){
        System.out.println("Laufzeit (in Jahren) des Sparvertrags: ");
        int jahre = scanner.nextInt();
        System.out.println("Wie viel Kapital (in Euro) möchten sie anlegen: ");
        int kapital = scanner.nextInt();
        System.out.println("Zinssatz: ");
        int zinsen = scanner.nextByte();
        System.out.println("Eingezahltes Kapital: "+kapital+" Euro");
        while(jahre>0){
            kapital *=zinsen;
            jahre--;
        }
        System.out.println("Ausgezahltes Kapital: "+ kapital+" Euro");
    }
    public static void aufgabe_6(Scanner scanner){
        boolean check = true;
        while(check){
            System.out.println("Bitte Kapital angeben: ");
            int kapital = scanner.nextInt();
            System.out.println("Bitte Zinssatz eingeben: ");
            int zinsen = scanner.nextInt();
            int jahre = 0;
            while(kapital<1000000){
                jahre++;
                kapital *=zinsen;
            }
            System.out.println("Nach "+jahre+" sind sie Millionaer");
            System.out.println("Möchten sie weitere Werte berechnen");
            String eingabe = scanner.nextLine();
            if(eingabe == "j"){

            }else{
                check = false;
            }
        }
    }
    public static void aufgabe_7(){
        double a = 0;
        double b = 250;
        while(a<1000&&b<1000){
            a +=9.5;
            b += 7;
            System.out.println("a: "+a+" b: "+b);
        }
        if(a>1000)
            System.out.println(a);
        else System.out.println(b);
    }
    public static void aufgabe_8(Scanner scanner){
        int eingabe;
        do {
            System.out.println("Bitte eine Zahl zwischen 2 und 9 eingeben. ");
            eingabe = scanner.nextInt();
            if(eingabe <2||eingabe >9) System.out.println("Ungueltige Eingabe.");
        }while(eingabe<2|| eingabe >9);

        ArrayList<String> zahlen = new ArrayList<>();
        int lauf =1;
        System.out.printf("%-3s",Integer.toString(0));
        while(lauf<100){
            if(lauf%eingabe==0||quersumme(lauf)%eingabe==0){
                System.out.printf("%-3s","*");;
            }
            else System.out.printf("%-3s",Integer.toString(lauf));
            lauf++;
            if(lauf%10==0) System.out.println();
        }

    }
    public static int quersumme(int zahl){
        int quer=0;
        while(zahl>0){
            quer+=zahl%10;
            zahl/=10;
        }
        return quer;
    }
}
