import java.util.ArrayList;
import java.util.Scanner;

public class For_Schleife {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Bitte eingeben welche Aufgabe bearbeitet werden soll(1-9)");
        int eingabe = scanner.nextInt();
        switch(eingabe) {
            case 1: zaehlen(scanner);
                break;
            case 2:summe(scanner);
                break;
            case 3: modulo();
                break;
            case 4: folgen();
                break;
            case 5: einmaleins();
                break;
            case 6: sterne(scanner);
                break;
            case 7: primzahlen();
                break;
            case 8: quadrat(scanner);
                break;
            case 9: treppe(scanner);
                break;
        }

    }

    public static void zaehlen(Scanner scanner){
        System.out.println("Bitte eine Zahl eingeben: ");
        int n = scanner.nextInt();
        hochzahlen(n);
        runterzahlen(n);
    }
    public static void hochzahlen(int n){
        for(int i=0;i<=n;i++){
            System.out.print(i+" ");
        }
        System.out.println();
    }
    public static void runterzahlen(int n){
        for(int i=n;i>=0;i--){
            System.out.print(i+" ");
        }
        System.out.println();
    }
    public static void summe(Scanner scanner){
        System.out.println("Geben sie bitte einen Begrenzenten Wert ein: ");
        int n = scanner.nextInt();
        int erg_eins = 0;
        int erg_zwei = 0;
        int erg_drei = 0;
        for(int i = 0;i<=n;i++){
            erg_eins += i;
            erg_zwei += 2*i;
            erg_drei += 2*i+1;
        }
        System.out.println("Die Summe für A beträgt: "+erg_eins);
        System.out.println("Die Summe für B beträgt: "+erg_zwei);
        System.out.println("Die Summe für C beträgt: "+erg_drei);
    }
    public static void modulo(){
        ArrayList<Integer> sieben = new ArrayList<>();
        ArrayList<Integer> vier = new ArrayList<>();
        for(int i = 1;i<=200;i++){
            if(i%7==0){
                sieben.add(i);
            }
            if(i%4==0&&i%5!=0){
                vier.add(i);
            }
        }
        System.out.println("Durch sieben teilbare Zahlen: ");
        for (int zahl: sieben) {
            System.out.print(zahl+" ");
        }
        System.out.println();
        System.out.println("Durch vier teilbare Zahlen: ");
        for (int zahl:vier) {
            System.out.print(zahl+" ");
        }
    }

    public static void folgen(){
        System.out.print("a: ");
        for(int i=99; i>=9;i=i-3){
            System.out.print(i+" ");
        }
        System.out.println();
        System.out.print("b: ");
        for(int i=1; i<=20;i++){
            System.out.print((i*i)+" ");
        }
        System.out.println();
        System.out.print("c: ");
        for(int i=2;i<=102;i+=4){
            System.out.print(i+" ");
        }
        System.out.println();
        System.out.print("d: ");
        int offset = 4;
        for(int i=offset;i<=1024;i=i+offset){
            System.out.print(i+" ");
            offset+=8;
        }
        System.out.println();
        System.out.print("e: ");
        for(int i=2;i<=32768;i<<=1){
            System.out.print(i+" ");
        }
    }
    public static void einmaleins(){
        System.out.println();
        System.out.println();
        for(int lauf = 0;lauf<=10;lauf++){
            if(lauf==0) {
                System.out.print("  ");
            }else{
                System.out.printf("%4d",lauf);
            }

        }
        System.out.println();
        for(int i=1;i<=10;i++){
            System.out.printf("%-2d",i);

            for(int j=1;j<=10;j++){
                System.out.printf("%4d",(i*j));
            }
            System.out.println();

        }
    }
    public static void sterne(Scanner scanner){
        System.out.println();
        String sternchen = "*";
        System.out.println("Bitte Anzahl der Sterne eingeben: ");
        int n = scanner.nextInt();
        for(int i=0;i<=n;i++){
            for(int j=i;j>=0;j--){
                System.out.print(sternchen);
            }
            System.out.println();
        }
    }

    public static void primzahlen(){
        ArrayList<Integer> primzahlen = new ArrayList<>();

        for(int i = 2;i<=100;i++){
            if(primzahlen.size()!=0){
                boolean check = true;
                for (int prim:primzahlen) {
                    if(i%prim==0){
                        check=false;
                        break;}
                }
                if(check)primzahlen.add(i);
            }
            else{primzahlen.add(i);
            }
        }
        System.out.println("Die Primzahlen bis 100 lauten: ");
        for(int prim:primzahlen) {
            System.out.print(prim+" ");
        }
    }
    public static void quadrat(Scanner scanner){
        System.out.println();
        int kantenlaenge = scanner.nextInt()-1;
        String stern = "*";
        for(int i=0;i<=kantenlaenge;i++){
            for(int j=0;j<=kantenlaenge;j++){
                if(i==0||i==kantenlaenge){
                    System.out.print(stern+" ");
                }else{
                    if(j==0||j==kantenlaenge){
                        System.out.print(stern+" ");
                    }else{
                        System.out.print("  ");
                    }
                }
            }
            System.out.println();
        }
    }
    public static void treppe(Scanner scanner){
        System.out.println("Bitte hoehe eingeben. ");
        int h = scanner.nextInt();
        System.out.println("Bitte breite eingeben. ");
        int b = scanner.nextInt();
        String stern = "*";
        for(int i = 0;i<=h;i++){
            for(int j=0;j<h*b;j++){
                if(j>=h*b-i*b){
                    System.out.print(stern);
                }else{
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

}
