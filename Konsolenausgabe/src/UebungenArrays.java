import java.util.Scanner;

public class UebungenArrays {
    public static void main(String[] args) {
       double matrix[][]={{2d,0d,1d},{0d,3d,4d},{1d,4d,14d}};
        //2 0 1
        //0 3 4
        //1 4 14
        Scanner sc = new Scanner(System.in);


        for(int iterator=0;iterator< matrix.length;iterator++) {
            for (int _iterator = 0; _iterator < matrix[0].length; _iterator++){
                einlesen(iterator, _iterator, matrix, sc);
                System.out.println(matrix[iterator][_iterator]);
            }
        }

       checkTransponierbar(matrix);
    }

    public static void checkTransponierbar(double matrix[][]){
        for(int iteratorX=0;iteratorX< matrix[0].length;iteratorX++){
                for(int iteratorXY=0; iteratorXY<matrix.length;iteratorXY++) {
                    if (matrix[iteratorXY][iteratorX] != matrix[iteratorX][iteratorXY]) {
                        System.out.println(matrix[iteratorXY][iteratorX]+" "+matrix[iteratorX][iteratorXY]);
                        System.out.println("Nicht zu sich selbst Transponierte Matrix");
                        return;
                    }
                }
        }
        System.out.println("Zu sich selbst Transponierte Matrix");
    }
    public static void einlesen(int m, int n, double matrix[][], Scanner sc){
        System.out.println("Bitte Wert fuer m ="+m+" und n ="+n+" eingeben");
        matrix[m][n]=sc.nextDouble();
    }
}
