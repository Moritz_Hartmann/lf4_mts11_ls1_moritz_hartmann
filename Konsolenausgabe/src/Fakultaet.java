public class Fakultaet {
  public static void main(String[] args) {
    String s = " 1 * 2 * 3 * 4 * 5 ";

    System.out.printf("0!   =%-19.1s=%4d\n",s,fakul(0));

    System.out.printf("1!   =%-19.3s=%4d\n",s,fakul(1));

    System.out.printf("2!   =%-19.6s=%4d\n",s,fakul(2));

    System.out.printf("3!   =%-19.11s=%4d\n",s,fakul(3));

    System.out.printf("4!   =%-19.14s=%4d\n",s,fakul(4));

    System.out.printf("5!   =%-19s=%4d\n",s,fakul(5));

  }

  public static int fakul(int i){
    if(i>0){
      return fakul(i-1)*i;
    }else{
      return 1;
    }
  }
}
